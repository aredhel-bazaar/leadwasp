(function() {
    'use strict';

    var lazyLoad;
    var charts = [];
    var chartsLength = 0;
    var cpuUsageCharts;
    var allAudioPlayers   = document.querySelectorAll('audio');
    var allPlayerWrappers = document.querySelectorAll('.audio-player-wrapper');

    window.onload = function () {
        console.debug("Starting...");

        lazyLoad = new LazyLoad();

        var textFileElm = document.querySelector('.js-textfile');
        if (textFileElm !== null) {
            var xhr         = new XMLHttpRequest();

            xhr.open('GET', textFileElm.dataset.src);

            xhr.onreadystatechange = function (event) {
                if (this.readyState === XMLHttpRequest.DONE) {
                    if (this.status === 200) {
                        textFileElm.textContent = this.responseText;
                        hljs.highlightBlock(textFileElm);
                    } else {
                        console.warn("Status de la réponse: %d (%s)", this.status, this.statusText);
                    }
                }
            };

            xhr.send(null);
        }

        var diskUsageCharts = document.querySelectorAll('.js-disk-usage-chart');
        if (diskUsageCharts.length) {
            diskUsageCharts.forEach(function(domChart, index, nodeList) {
                var freeSpace  = domChart.dataset.freespace;
                var usedSpace  = domChart.dataset.usedspace;
                var partition  = domChart.dataset.partition;
                var mountPoint = domChart.dataset.mountpoint;
                var totalSpace = domChart.dataset.totalspace;

                var chart = echarts.init(domChart);

                chart.setOption({
                    title: [{
                        left: 'center',
                        text: mountPoint
                    }],
                    legend: {
                        data: ['Used space', 'Free space', 'Total space'],
                        top: 24
                    },
                    series: [
                        {
                            name: partition,
                            type: 'pie',
                            radius: ['50%', '70%'],
                            avoidLabelOverlap: false,
                            label: {
                                normal: {
                                    show: false,
                                    position: 'center'
                                },
                                emphasis: {
                                    show: true,
                                    textStyle: {
                                        fontSize: '16',
                                        fontWeight: 'bold'
                                    }
                                }
                            },
                            data: [
                                {
                                    value: usedSpace,
                                    name: 'Used space',
                                    label: {
                                        formatter: '{d}% \n ' + getReadableFileSizeString(usedSpace)
                                    }
                                },
                                {
                                    value: freeSpace,
                                    name: 'Free space',
                                    label: {
                                        formatter: '{d}% \n ' + getReadableFileSizeString(freeSpace)
                                    }
                                }
                            ]
                        },
                        {
                            name: partition,
                            type: 'pie',
                            radius: ['70%', '80%'],
                            avoidLabelOverlap: false,
                            label: {
                                normal: {
                                    show: false,
                                    position: 'center'
                                },
                                emphasis: {
                                    show: true,
                                    textStyle: {
                                        fontSize: '16',
                                        fontWeight: 'bold'
                                    }
                                }
                            },
                            data: [
                                {
                                    value: totalSpace,
                                    name: 'Total space',
                                    label: {
                                        formatter: getReadableFileSizeString(totalSpace)
                                    }
                                }
                            ]
                        }
                    ]
                });
            });
        }

        var memoryUsageChart = document.querySelector('.js-memory-usage-chart');
        if (memoryUsageChart !== null) {
            chartsLength    = charts.push(echarts.init(memoryUsageChart));
            var memoryChart = charts[chartsLength-1];
            var usedSpace   = memoryUsageChart.dataset.used;
            var totalSpace  = memoryUsageChart.dataset.total;

            memoryChart.setOption({
                title: [{
                    left: 'center',
                    text: 'Memory'
                }],
                tooltip : {
                    trigger: 'item',
                    axisPointer : {
                        type : 'shadow'
                    }
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                yAxis:  {
                    type: 'value',
                    max: 100
                },
                xAxis: {
                    type: 'category',
                    data: ['Used memory']
                },
                series: [
                    {
                        name: 'Used memory',
                        type: 'bar',
                        stack: '1',
                        data: [(usedSpace / totalSpace * 100).toFixed(2)]
                    }
                ]
            });

            updateMemoryChart(memoryChart);
        }

        cpuUsageCharts = document.querySelectorAll('.js-cpu-usage-chart');
        if (cpuUsageCharts.length) {
            cpuUsageCharts.forEach(function(domChart, index, nodeList) {
                var chart      = echarts.init(domChart);
                var usedSpace  = domChart.dataset.used;

                chart.setOption({
                    title: [{
                        left: 'center',
                        text: 'Core'+index
                    }],
                    tooltip : {
                        trigger: 'item',
                        axisPointer : {
                            type : 'shadow'
                        }
                    },
                    grid: {
                        left: '3%',
                        right: '4%',
                        bottom: '3%',
                        containLabel: true
                    },
                    yAxis:  {
                        type: 'value',
                        max: 100
                    },
                    xAxis: {
                        type: 'category',
                        data: ['Core'+index]
                    },
                    series: [
                        {
                            name: 'Activity',
                            type: 'bar',
                            stack: '1',
                            data: [usedSpace]
                        }
                    ]
                });

            });

            updateCpuCharts();
        }


        var switchViewElm = document.querySelector('.js-folder-switch-view');
        if (switchViewElm !== null) {
            switchViewElm.addEventListener('click', function(event) {
                event.preventDefault();

                var folderElm = document.querySelector('.js-folder');
                var pathHash  = sha1(folderElm.dataset.path);

                folderElm.classList.toggle("show-list");
                folderElm.classList.toggle("show-icons");

                switchViewElm.querySelector('.icon').classList.toggle('active');
                switchViewElm.querySelector('.list').classList.toggle('active');

                setCookie(pathHash, (folderElm.classList.contains("show-list") ? "show-list" : "show-icons"), 90);

                console.debug("Set cookie with key "+pathHash)
            });
        }

        var audioListElm = document.querySelectorAll('a[data-mimetype^="audio/"] .thumbnail-wrapper');
        if (audioListElm.length) {
            audioListElm.addEventListener('click', function(event) {
                event.preventDefault();
                event.stopPropagation();

                var doStart          = true;
                var playerWrapperElm = event.target.parent('div.item').querySelector('.audio-player-wrapper');
                var audioPlayerElm   = playerWrapperElm.querySelector('audio');

                allPlayerWrappers.forEachOf('.active', function(elm) {
                    if (elm.classList.contains('active')) {
                        elm.classList.add('hidden');
                        elm.classList.remove('active');

                        if (elm === playerWrapperElm) {
                            doStart = false;
                        }
                    }
                });

                allAudioPlayers.forEach(function(elm) {
                    elm.pause();
                    elm.currentTime = 0;
                });

                if (doStart) {
                    playerWrapperElm.classList.toggle('hidden');
                    playerWrapperElm.classList.toggle('active');
                    audioPlayerElm.play();
                }
            });
        }
    };

    window.onunload = function (ev) {
        if (lazyLoad.xhr !== undefined) {
            lazyLoad.xhr.abort();
        }
    };

    var getReadableFileSizeString = function(fileSizeInBytes) {
        var i = -1;
        var byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
        do {
            fileSizeInBytes = fileSizeInBytes / 1024;
            i++;
        } while (fileSizeInBytes > 1024);

        return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
    };

    var updateCpuCharts = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', '/dashboard/cpu');

        xhr.onreadystatechange = function (event) {
            if (this.readyState === XMLHttpRequest.DONE) {
                if (this.status === 200) {
                    var responseText = JSON.parse(this.responseText);

                    cpuUsageCharts.forEach(function(domChart, index, nodeList) {
                        if (responseText.cpu[index] !== undefined) {
                            echarts.getInstanceByDom(domChart).setOption({
                                series: [
                                    {
                                        name: 'Core'+index,
                                        data: [responseText.cpu[index]]
                                    }
                                ]
                            });
                        }
                    });

                    setTimeout(function() {
                        updateCpuCharts();
                    }, 1000);
                }
            }
        };

        xhr.send(null);
    };

    var updateMemoryChart = function(memoryChart) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', '/dashboard/memory');

        xhr.onreadystatechange = function (event) {
            if (this.readyState === XMLHttpRequest.DONE) {
                if (this.status === 200) {
                    var responseText = JSON.parse(this.responseText);

                    memoryChart.setOption({
                        series: [
                            {
                                name: 'Used memory',
                                data: [(responseText.memory.used / responseText.memory.total * 100).toFixed(2)]
                            }
                        ]
                    });

                    setTimeout(function() {
                        updateMemoryChart(memoryChart);
                    }, 1000);
                }
            }
        };

        xhr.send(null);
    };
})();
