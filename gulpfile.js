'use strict';

var gulp      = require('gulp'),
    compass   = require('gulp-compass'),
    minifyCSS = require('gulp-minify-css');
const del   = require('del');
var config = {};

/** START : Arguments to Config */
process.argv.forEach(function(argument) {
    if (argument.charAt(0) === '-') {
        var argumentKey   = '';
        var argumentValue = '';

        if (argument.charAt(1) === '-') {
            argumentKey   = argument.slice(2).replace(/(\-\w)/g, function(m){return m[1].toUpperCase();});
            argumentValue = true;
        } else {
            var splittedArgument = argument.split('=');
            argumentKey          = splittedArgument[0].slice(1).replace(/(\-\w)/g, function(m){return m[1].toUpperCase();});
            argumentValue        = splittedArgument[1];
        }

        config[argumentKey] = argumentValue;
    }
});
/** END : Arguments to Config */

var tasks = [
    'clean', 'media', 'sass', 'javascript', 'libraries'
];

if (!config.noWatch) {
    tasks.push('sass:watch', 'javascript:watch');
}

gulp.task('sass', function () {
    gulp.src('assets/scss/*.scss')
        .pipe(compass({
            config_file: './config.rb',
            css: 'public/static/css',
            sass: 'assets/scss'
        }))
        .pipe(minifyCSS())
        .pipe(gulp.dest('public/static/css'))
    ;
});

gulp.task('sass:watch', function () {
    gulp.watch('assets/scss/**/*.scss', ['sass']);
});

gulp.task('media', function () {
    gulp
        .src(['assets/img/**/*'])
        .pipe(gulp.dest('public/static/img'))
    ;
});

gulp.task('javascript', function () {
    gulp.src('assets/js/**/*.js')
        .pipe(gulp.dest('public/static/js'))
    ;
});

gulp.task('libraries', function () {
    gulp.src('node_modules/video.js/dist/**/*')
        .pipe(gulp.dest('public/static/lib/video.js'))
    ;

    gulp.src('node_modules/videojs-swf/dist/video-js.swf')
        .pipe(gulp.dest('public/static/lib/video.js'))
    ;

    gulp.src('node_modules/videojs-flash/dist/videojs-flash.min.js')
        .pipe(gulp.dest('public/static/lib/videojs-flash'))
    ;

    gulp.src('node_modules/videojs-flvjs/dist/**/*')
        .pipe(gulp.dest('public/static/lib/videojs-flvjs'))
    ;

    gulp.src('node_modules/echarts/dist/**/*')
        .pipe(gulp.dest('public/static/lib/echarts'))
    ;

    gulp.src('node_modules/js-sha1/build/**/*')
        .pipe(gulp.dest('public/static/lib/js-sha1'))
    ;

    gulp.src('node_modules/requirejs/require.js')
        .pipe(gulp.dest('public/static/lib/requirejs'))
    ;
});

gulp.task('javascript:watch', function () {
    gulp.watch('assets/js/**/*.js', ['javascript']);
});

gulp.task('clean', function () {
    del.sync([
        'public/static/*'
    ]);
});

/** Run the tasks */
gulp.task('default', tasks);
