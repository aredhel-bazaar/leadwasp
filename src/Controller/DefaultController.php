<?php

namespace App\Controller;

use App\Service\SshAccessService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 * @package App\Controller
 *
 * @Route("/")
 */
class DefaultController extends Controller
{
    /**
     * @Route("", name="homepage")
     *
     * @param Request          $request
     * @param SshAccessService $accessService
     * @param AdapterInterface $cacheAdapter
     *
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function indexAction(Request $request, SshAccessService $accessService, AdapterInterface $cacheAdapter)
    {
        $path         = $request->get('path', '/');
        $cachedFolder = $cacheAdapter->getItem('path_' . sha1($path));
        $folder       = $cachedFolder->get();

        if (empty($folder['children'])) {
            try {
                $folder['children'] = $accessService->listFolder($path);

                $cachedFolder->set($folder);
                $cacheAdapter->save($cachedFolder);
            } catch (\Exception $e) {
                return new RedirectResponse($this->generateUrl('homepage'));
            }
        }

        return $this->render("homepage.html.twig", [
            'path'   => $path,
            'folder' => $cachedFolder->get()
        ]);
    }
}
