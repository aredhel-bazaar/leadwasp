<?php

namespace App\Controller;

use App\Service\ActivityMonitor;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DashboardController
 * @package App\Controller
 *
 * @Route("/dashboard")
 */
class DashboardController extends Controller
{
    /**
     * @Route("/", name="dashboard_index")
     *
     * @param ActivityMonitor $activityMonitor
     *
     * @return Response
     */
    public function index(ActivityMonitor $activityMonitor)
    {
        $diskUsageEntries = $activityMonitor->getDisksUsage();
        $memoryUsageEntry = $activityMonitor->getMemoryUsage();

        return $this->render('dashboard.html.twig', [
            'diskEntries' => $diskUsageEntries,
            'memoryEntry' => $memoryUsageEntry,
            'cpuEntries'  => $activityMonitor->getCPUUsage(true)
        ]);
    }

    /**
     * @Route("/cpu", name="dashboard_cpu")
     *
     * @param ActivityMonitor $activityMonitor
     *
     * @return JsonResponse
     */
    public function cpuUsage(ActivityMonitor $activityMonitor)
    {
        return new JsonResponse(['cpu' => $activityMonitor->getCPUUsage()]);
    }

    /**
     * @Route("/memory", name="dashboard_memory")
     *
     * @param ActivityMonitor $activityMonitor
     *
     * @return JsonResponse
     */
    public function memoryUsage(ActivityMonitor $activityMonitor)
    {
        return new JsonResponse(['memory' => $activityMonitor->getMemoryUsage()]);
    }
}