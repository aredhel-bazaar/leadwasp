<?php

namespace App\Controller;

use App\Service\SshAccessService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MediaController
 * @package App\Controller
 *
 * @Route("/media")
 */
class MediaController extends Controller
{
    /**
     * @Route("/thumbnail/{pathHash}.jpg", name="display-thumbnail")
     *
     * @param SshAccessService $accessService
     * @param LoggerInterface  $logger
     * @param AdapterInterface $cacheAdapter
     * @param string           $pathHash
     *
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function thumbnailAction(SshAccessService $accessService, LoggerInterface $logger, AdapterInterface $cacheAdapter, string $pathHash)
    {
        $cachedPath = $cacheAdapter->getItem('path_'.$pathHash);
        $pathInfo   = $cachedPath->get();

        if (isset($pathInfo['status']) && $pathInfo['status'] == 'ERROR') {
            return new Response(null, Response::HTTP_NOT_FOUND);
        }

        try {
            $thumbnailPath      = $accessService->getThumbnail($pathInfo['path']);
            $pathInfo['status'] = 'OK';

            $cachedPath->set($pathInfo);
            $cacheAdapter->saveDeferred($cachedPath);
        } catch (\Exception $e) {
            $logger->error($e->getMessage(), ['exception' => $e]);

            $pathInfo['status'] = 'ERROR';

            $cachedPath->set($pathInfo);
            $cacheAdapter->saveDeferred($cachedPath);

            return new Response(null, Response::HTTP_NOT_FOUND);
        }

        return new BinaryFileResponse($thumbnailPath);
    }



    /**
     * @Route("/detail", name="display-media")
     *
     * @param Request          $request
     * @param SshAccessService $accessService
     *
     * @param LoggerInterface  $logger
     *
     * @return Response
     */
    public function displayAction(Request $request, SshAccessService $accessService, LoggerInterface $logger)
    {
        $path   = $request->get('path', '/');
        
        try {
            $media    = $accessService->getMedia($path);
            $media->setSupportLevel($this->getUser()->getUserAgent()->supportsMimeType($media->getMimeType()));
        } catch (\Exception $e) {
            $logger->error($e->getMessage());
            return new Response('', Response::HTTP_NOT_FOUND);
        }

        return $this->render("media/".$media->getType().".html.twig", [
            'path'  => $path,
            'media' => $media
        ]);
    }



    /**
     * @Route("/play", name="stream-media")
     *
     * @param Request          $request
     * @param SshAccessService $accessService
     *
     * @return BinaryFileResponse
     * @throws \Exception
     */
    public function getMediaAction(Request $request, SshAccessService $accessService)
    {
        $path            = $request->get('path', '/');
        $mediaStreamPath = $accessService->getMediaStreamPath($path);

        return new BinaryFileResponse($mediaStreamPath);
    }
}
