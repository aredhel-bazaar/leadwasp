<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\LoginForm;
use App\Service\SshAccessService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class UserController
 * @package App\Controller
 *
 * @Route("/security")
 */
class UserController extends Controller
{
    /**
     * @Route("/login", name="security_login")
     *
     * @param AuthenticationUtils      $authUtils
     *
     * @return Response
     */
    public function loginAction(AuthenticationUtils $authUtils)
    {
        $error        = $authUtils->getLastAuthenticationError();
        $lastUsername = $authUtils->getLastUsername();
        $user         = new User();
        $user->setUsername($lastUsername);
        $form = $this->createForm(LoginForm::class, $user);
    
        return $this->render('login.html.twig', array(
            'error'         => $error,
            'form'          => $form->createView()
        ));
    }
    
    
    
    /**
     * @Route("/logout", name="security_logout")
     */
    public function logoutAction()
    {
        throw new AccessDeniedHttpException("You should have not seen this.");
    }
}
