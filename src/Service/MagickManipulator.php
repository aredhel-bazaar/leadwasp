<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\File;

class MagickManipulator
{
    private $width;
    private $height;

    private $maxHeight;
    private $maxWidth;

    private $newHeight;
    private $newWidth;
    private $x;
    private $y;
    /**
     * @var string
     */
    private $projectDir;
    /**
     * @var bool
     */
    private $allowCrop;
    /**
     * @var \Imagick
     */
    private $imagick;
    /**
     * @var string
     */
    private $filepath;
    /**
     * @var bool
     */
    private $optimizeSize;


    /**
     * ImageManipulator constructor.
     *
     * @param int         $maxHeight
     * @param int         $maxWidth
     * @param string      $projectDir
     * @param File        $file
     * @param null|string $filename
     *
     * @throws \ImagickException
     */
    public function __construct(int $maxHeight, int $maxWidth, string $projectDir, File $file, ?string $filename)
    {
        $this->maxHeight  = $maxHeight;
        $this->maxWidth   = $maxWidth;
        $this->projectDir = $projectDir;
        $this->imagick    = new \Imagick($file->getRealPath());

        $this->imagick->setImageFilename($filename ?? uniqid() . '.' . $file->getExtension());
        $this->width  = $this->imagick->getImageWidth();
        $this->height = $this->imagick->getImageHeight();
    }


    /**
     * @param int $maxHeight
     *
     * @return $this
     */
    public function setMaxHeight(?int $maxHeight)
    {
        $this->maxHeight = $maxHeight;

        return $this;
    }


    /**
     * @param int $maxWidth
     *
     * @return $this
     */
    public function setMaxWidth(?int $maxWidth)
    {
        $this->maxWidth = $maxWidth;

        return $this;
    }


    /**
     * @param bool $allowCrop
     *
     * @param bool $optimizeSize
     *
     * @return $this
     * @throws \Exception
     */
    public function resize($allowCrop = true, $optimizeSize = true)
    {
        $this->allowCrop    = $allowCrop;
        $this->optimizeSize = $optimizeSize;

        if (is_null($this->maxWidth)) {
            $this->setMaxWidth($this->width);
        }

        if (is_null($this->maxHeight)) {
            $this->setMaxHeight($this->height);
        }

        $this->defineNewDimensions();

        $this->processResize();

        return $this;
    }


    /**
     * @param $path
     *
     * @return $this
     * @throws \Exception
     */
    public function move($path)
    {
        $filepath = $this->projectDir . DIRECTORY_SEPARATOR . $path;

        if (!is_dir($filepath)) {
            mkdir($filepath, 0777, true);
        }

        $this->filepath = $filepath . DIRECTORY_SEPARATOR . $this->imagick->getImageFilename();

        if (!$this->imagick->writeImage($this->filepath)) {
            throw new \Exception("Unable to write image");
        }

        return $this;
    }


    /**
     * @return mixed
     */
    public function getResizedImageHeight()
    {
        return $this->newHeight;
    }


    /**
     * @return mixed
     */
    public function getResizedImageWidth()
    {
        return $this->newWidth;
    }


    /**
     * @return mixed
     */
    public function getResizedImagePath()
    {
        return $this->filepath;
    }


    /**
     *
     */
    private function defineNewDimensions()
    {
        $this->newHeight = $this->maxHeight;
        $this->newWidth  = $this->maxWidth;
        $x               = 0;
        $y               = 0;
        $ratioHaut       = $this->maxHeight / $this->height;
        $ratioLarg       = $this->maxWidth / $this->width;

        if ($this->width > $this->maxWidth && $this->height > $this->maxHeight) {
            if ($ratioHaut >= $ratioLarg) {
                $this->newWidth = $this->width * $ratioHaut;
                $x              = round(($this->maxWidth - $this->newWidth) / 2);
            } else {
                $this->newHeight = $this->height * $ratioLarg;
                $y               = round(($this->maxHeight - $this->newHeight) / 2);
            }
        } elseif ($this->width >= $this->maxWidth && $this->height <= $this->maxHeight) {
            if ($this->allowCrop) {
                $y = round(($this->maxHeight - $this->height) / 2);
            } else {
                $this->newHeight = $this->height * $ratioLarg;
            }
        } elseif ($this->width <= $this->maxWidth && $this->height >= $this->maxHeight) {
            if ($this->allowCrop) {
                $x = round(($this->maxWidth - $this->width) / 2);
            } else {
                $this->newWidth = $this->width * $ratioHaut;
            }
        } elseif ($this->width <= $this->maxWidth && $this->height <= $this->maxHeight) {
            $this->newWidth  = $this->width;
            $this->newHeight = $this->height;
            $x               = round(($this->width - $this->maxWidth) / 2);
            $y               = round(($this->height - $this->maxHeight) / 2);
        }

        $this->x = $x;
        $this->y = $y;
    }


    /**
     * @throws \Exception
     */
    private function processResize()
    {
        try {
            $this->imagick->adaptiveResizeImage($this->newWidth, $this->newHeight);

            if ($this->allowCrop) {
                $this->imagick->cropImage($this->maxWidth, $this->maxHeight, abs($this->x), abs($this->y));
            }

            if ($this->optimizeSize) {
                switch ($this->imagick->getImageMimeType()) {
                    case 'image/jpg':
                    case 'image/jpeg':
                        $this->imagick->setAntiAlias(true);
                        $this->imagick->setImageCompression(\Imagick::COMPRESSION_JPEG);
                        $this->imagick->setImageCompressionQuality(90);
                        break;

                    case 'image/png':
                    case 'image/gif':
                        $this->imagick->setAntiAlias(true);
                        $this->imagick->setImageCompression(\Imagick::COMPRESSION_ZIP);
                        $this->imagick->setImageCompressionQuality(75);
                        break;
                }
            }
        } catch (\Exception $e) {
            throw new \Exception("Unable to resize source image");
        }
    }


    /**
     * @return mixed
     */
    public function getResizedImageWebPath()
    {
        return str_replace($this->projectDir . '/public', '', $this->getResizedImagePath());
    }
}
