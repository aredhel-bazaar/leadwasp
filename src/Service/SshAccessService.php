<?php

namespace App\Service;

use App\Entity\Media;
use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SshAccessService
{
    /**
     * @var resource
     */
    private $sftpSession;
    /**
     * @var resource
     */
    private $sshTunnel;
    /**
     * @var TokenStorageInterface
     */
    private $token;
    /**
     * @var CacheItemPoolInterface
     */
    private $cache;
    /**
     * @var string
     */
    private $cacheDir;
    /**
     * @var string
     */
    private $ffmpeg;
    /**
     * @var string
     */
    private $ffprobe;
    /**
     * @var RsaService
     */
    private $rsaService;

    const THUMBNAIL_CACHE_KEY = 'media_thumbnail';
    const THUMBNAIL_CACHE_DIR = 'thumbnails';
    const TMP_THUMBNAIL_DIR   = '/tmp/videoThumbnail';

    const BASH_MONTHS = [
        'jan.'  => 1,
        'fév.'  => 2,
        'mars'  => 3,
        'avr.'  => 4,
        'mai'   => 5,
        'juin'  => 6,
        'juil.' => 7,
        'août'  => 8,
        'sept.' => 9,
        'oct.'  => 10,
        'nov.'  => 11,
        'déc.'  => 12,
    ];
    /**
     * @var string
     */
    private $projectDir;


    /**
     * SshAccessService constructor.
     *
     * @param TokenStorageInterface  $token
     * @param RsaService             $rsaService
     * @param CacheItemPoolInterface $cache
     * @param string                 $cacheDir
     * @param string                 $projectDir
     */
    public function __construct(TokenStorageInterface $token, RsaService $rsaService, CacheItemPoolInterface $cache, string $cacheDir, string $projectDir)
    {
        $this->token      = $token;
        $this->cache      = $cache;
        $this->cacheDir   = $cacheDir;
        $this->rsaService = $rsaService;
        $this->projectDir = $projectDir;
    }


    /**
     * @return resource
     */
    private function getSshTunnel()
    {
        if (!is_resource($this->sshTunnel)) {
            $this->sshTunnel = ssh2_connect('localhost', 22);
            ssh2_auth_password(
                $this->sshTunnel,
                $this->token->getToken()->getUser()->getUsername(),
                $this->rsaService->decrypt($this->token->getToken()->getUser()->getPassword())
            );
            setlocale(LC_CTYPE, "fr_FR.UTF-8");
        }

        return $this->sshTunnel;
    }


    /**
     * @throws \Exception
     */
    private function getFfmpegPath()
    {
        if (empty($this->ffmpeg)) {
            $result = ssh2_exec($this->getSshTunnel(), 'which ffmpeg');

            stream_set_blocking($result, true);
            $result = stream_get_contents($result);

            if (empty($result)) {
                throw new \Exception("ffmpeg seams not to be installed in your system, or is not executable by current user.");
            }

            $this->ffmpeg = explode(PHP_EOL, $result, -1)[0];
        }

        return $this->ffmpeg;
    }


    /**
     * @throws \Exception
     */
    private function getFfProbePath()
    {
        if (empty($this->ffprobe)) {
            $result = ssh2_exec($this->getSshTunnel(), 'which ffprobe');

            stream_set_blocking($result, true);
            $result = stream_get_contents($result);

            if (empty($result)) {
                throw new \Exception("ffprobe seams no to be installed in your system, or is not executable by current user.");
            }

            $this->ffprobe = explode(PHP_EOL, $result, -1)[0];
        }

        return $this->ffprobe;
    }


    /**
     * @return resource
     */
    private function getSftpConnection()
    {
        if (!is_resource($this->sftpSession)) {
            $this->sftpSession = ssh2_sftp($this->getSshTunnel());
        }

        return $this->sftpSession;
    }


    /**
     * @return string
     */
    public function getRootPath()
    {
        return '/nas';
    }


    /**
     * @param string $path
     *
     * @return array
     * @throws \Exception
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function listFolder(string $path)
    {
        $this->getSftpConnection();

        $exec = ssh2_exec($this->getSshTunnel(), 'ls "' . $this->getRootPath() . $path . '" -Al --full-time');
        stream_set_blocking($exec, true);
        $dir = explode(PHP_EOL, stream_get_contents($exec), -1);
        array_shift($dir);

        $entries = [];
        foreach ($dir as $entry) {

            $entry = explode(" ", $entry);
            $entry = array_values(array_filter($entry, 'trim'));

            $permissions    = array_shift($entry);
            $nbLinks        = array_shift($entry);
            $owner          = array_shift($entry);
            $group          = array_shift($entry);
            $size           = array_shift($entry);
            $updateDateTime = array_shift($entry) . ' ' . (explode('.', array_shift($entry))[0]) . ' ' . array_shift($entry);
            $entry          = implode(" ", $entry);

            if (!in_array($entry, ['.', '..', 'lost+found']) && substr($entry, 0, 1) !== '.') {
                $mediaPath    = $path . ($path !== '/' ? '/' : '') . $entry;
                $mimeType     = $this->getMimeType($this->getRootPath() . $mediaPath);
                $entryPathSum = sha1($mediaPath);

                $cachedPath = $this->cache->getItem('path_' . $entryPathSum);

                $pathInfo   = [
                    'status'  => 'PENDING',
                    'path'    => $mediaPath,
                    'entry'   => new Media(
                        [
                            'title'            => $metadata['title'] ?? pathinfo($mediaPath, PATHINFO_FILENAME),
                            'path'             => $mediaPath,
                            'filename'         => pathinfo($this->getRootPath() . $mediaPath, PATHINFO_FILENAME) . '.' . pathinfo($this->getRootPath() . $path, PATHINFO_EXTENSION),
                            'mimeType'         => $mimeType,
                            'modificationDate' => \DateTime::createFromFormat('Y-m-d H:i:s O', $updateDateTime),
                            'meta'             => [
                                'sum' => $entryPathSum
                            ]
                        ]
                    ),
                    'children' => []
                ];

                $cachedPath->set($pathInfo);
                $this->cache->saveDeferred($cachedPath);

                $entries[] = $pathInfo;
            }
        }

        return $entries;
    }


    /**
     * @param string $path
     *
     * @return string
     *
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Exception
     */
    public function getThumbnail(string $path)
    {
        $this->getSftpConnection();

        $cachedThumbnails = $this->cache->getItem(self::THUMBNAIL_CACHE_KEY);
        $thumbnails       = $cachedThumbnails->get() ?? [];
        $pathSum          = sha1($path);
        $fileExt          = pathinfo($path, PATHINFO_EXTENSION);

        if (!$cachedThumbnails->isHit() || empty($thumbnails) || empty($thumbnails[$pathSum]) || !file_exists($thumbnails[$pathSum])) {
            $media = $this->getMedia($path);

            $streamedFileExists = ssh2_exec($this->getSshTunnel(), "test -e " . escapeshellarg($this->getRootPath() . $path) . " && echo 1 || echo 0");
            stream_set_blocking($streamedFileExists, true);

            if (stream_get_contents($streamedFileExists) == 1) {
                if (!is_dir($this->cacheDir . '/' . self::THUMBNAIL_CACHE_DIR)) {
                    if (is_writeable(!is_dir($this->cacheDir))) {
                        throw new \Exception("The cache directory is not writeable");
                    }
                    mkdir($this->cacheDir . '/' . self::THUMBNAIL_CACHE_DIR);
                }

                $distantThumbnail = $this->getRootPath() . $media->getPath();

                if ($media->isVideo()) {
                    $videoDuration     = $this->getMedia($path)->getDuration();
                    $thumbnailPosition = $videoDuration * 0.01;

                    $hours   = str_pad(floor($thumbnailPosition / 3600), 2, '0', STR_PAD_LEFT);
                    $minutes = str_pad(floor(($thumbnailPosition - $hours * 3600) / 60), 2, '0', STR_PAD_LEFT);
                    $seconds = str_pad(floor(($thumbnailPosition - ($hours * 3600 + $minutes * 60))), 2, '0', STR_PAD_LEFT);

                    if (!is_dir(self::TMP_THUMBNAIL_DIR)) {
                        mkdir(self::TMP_THUMBNAIL_DIR);
                    }
                    $fileExt          = 'jpg';
                    $distantThumbnail = self::TMP_THUMBNAIL_DIR . '/' . $pathSum . '.' . $fileExt;
                    $result           = ssh2_exec($this->getSshTunnel(), $this->getFfmpegPath() . " -i " . escapeshellarg($this->getRootPath() . $media->getPath()) . " -ss $hours:$minutes:$seconds -vframes 1 " . $distantThumbnail . " -y");
                    stream_set_blocking($result, true);
                    stream_get_contents($result);

                    if (!file_exists($distantThumbnail)) {
                        throw new \Exception("Unable to generate the media thumbnail.");
                    }
                }

                if ($media->isDirectory()) {
                    $distantThumbnail .= '/folder.jpg';
                }

                $thumbnail = $this->cacheDir . '/' . self::THUMBNAIL_CACHE_DIR . '/' . $pathSum . '.' . $fileExt;
                ssh2_scp_recv($this->getSshTunnel(), $distantThumbnail, $thumbnail);

                $manipulator = new MagickManipulator(200, 200, $this->projectDir, new File($thumbnail), $pathSum . '.thumb' . '.' . $fileExt);
                $manipulator->resize(false);
                $manipulator->move('var/cache/' . self::THUMBNAIL_CACHE_DIR . '/');

                $thumbnails[$pathSum] = $manipulator->getResizedImagePath();
                $cachedThumbnails->set($thumbnails);
                $this->cache->save($cachedThumbnails);
            }
        }

        return $thumbnails[$pathSum] ?? null;
    }


    /**
     * @param string $path
     *
     * @return Media
     * @throws \Exception
     */
    public function getMedia(string $path)
    {
        $streamedFileExists = ssh2_exec($this->getSshTunnel(), "[[ -f " . escapeshellarg($this->getRootPath() . $path) . " || -d " . escapeshellarg($this->getRootPath() . $path) . " ]] && echo 1 || echo 0");
        stream_set_blocking($streamedFileExists, true);

        if (stream_get_contents($streamedFileExists) != 1) {
            throw new \Exception("File does not exist");
        }

        $mimeType = $this->getMimeType($this->getRootPath() . $path);
        $metadata = [
            'sum' => sha1($path)
        ];

        if ($mimeType !== 'inode/directory') {
            $streamedOutput = ssh2_exec($this->getSshTunnel(), $this->getFfProbePath() . " " . escapeshellarg($this->getRootPath() . $path) . " -hide_banner 2>&1");
            stream_set_blocking($streamedOutput, true);
            $output = stream_get_contents($streamedOutput);

            $rawMetadata = explode(PHP_EOL, $output);

            foreach ($rawMetadata as $metadatum) {
                $explodedMetadatum = explode(':', $metadatum, -1);

                if (!empty($explodedMetadatum[1])) {
                    $key   = mb_strtolower(trim($explodedMetadatum[0]));
                    $value = trim($explodedMetadatum[1]);

                    if (count($explodedMetadatum) > 2) {
                        if ($key === 'duration') {
                            $hours   = (int)$explodedMetadatum[1];
                            $minutes = (int)$explodedMetadatum[2];
                            $seconds = (int)explode('.', $explodedMetadatum[3])[0];

                            if (!empty($explodedMetadatum[5])) {
                                $metadata['bitrate'] = trim($explodedMetadatum[5]);
                            }

                            $value = $hours * 3600 + $minutes * 60 + $seconds;
                        } elseif ($key === 'stream #0') {
                            continue;
                        }
                    }

                    $metadata[$key] = $value;
                }
            }

            $regex_sizes = "/Video: ([^,]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/"; // or : $regex_sizes = "/Video: ([^\r\n]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/"; (code from @1owk3y)
            if (preg_match($regex_sizes, $output, $regs)) {
                $codec  = $regs[1] ? $regs[1] : null;
                $width  = $regs[3] ? $regs[3] : null;
                $height = $regs[4] ? $regs[4] : null;
            }

            // File encoding
            $streamedEncoding = ssh2_exec($this->getSshTunnel(), 'file -i ' . escapeshellarg($this->getRootPath() . $path));
            stream_set_blocking($streamedEncoding, true);
            $encodingOutput = stream_get_contents($streamedEncoding);
            if (isset($encodingOutput)) {
                $ex       = explode('charset=', $encodingOutput);
                $encoding = $ex[1] ?? null;
            }
        }

        return new Media($metadata + [
                'title'    => $metadata['title'] ?? pathinfo($path, PATHINFO_FILENAME),
                'path'     => $path,
                'filename' => pathinfo($this->getRootPath() . $path, PATHINFO_FILENAME) . '.' . pathinfo($this->getRootPath() . $path, PATHINFO_EXTENSION),
                'mimeType' => $mimeType,
                'codec'    => $codec ?? null,
                'width'    => $width ?? null,
                'height'   => $height ?? null,
                'filesize' => @filesize($this->getRootPath() . $path),
                'encoding' => $encoding ?? null,
            ]);
    }


    /**
     * @param string $path
     *
     * @return string
     */
    private function getMimeType(string $path)
    {
        $streamedMimeType = ssh2_exec($this->getSshTunnel(), "file -b --mime-type " . escapeshellarg($path));
        stream_set_blocking($streamedMimeType, true);

        return trim(stream_get_contents($streamedMimeType));
    }


    /**
     * @param Media|string $media
     *
     * @return bool|resource
     */
    public function getMediaStream($media)
    {
        return fopen($this->getMediaStreamPath($media), 'rb');
    }


    /**
     * @param Media|string $media
     *
     * @return bool|resource
     */
    public function getMediaStreamPath($media)
    {
        return "ssh2.sftp://" . intval($this->getSftpConnection()) . $this->getRootPath() . ($media instanceof Media ? $media->getPath() : $media);
    }
}
