<?php

namespace App\Service;

use App\Security\SshRootAccess;

class ActivityMonitor
{
    const PARTITIONS = [
        '/',
        '/nas/nas1',
        '/nas/nas2',
        '/nexus/srv',
    ];

    /**
     * @var SshRootAccess
     */
    private $rootAccess;

    /**
     * ActivityMonitor constructor.
     *
     * @param SshRootAccess $rootAccess
     */
    public function __construct(SshRootAccess $rootAccess)
    {
        $this->rootAccess = $rootAccess;
    }

    /**
     * @return array
     */
    public function getDisksUsage()
    {
        $sshTunnel = $this->rootAccess->getSshTunnel();

        $streamedResponse = ssh2_exec($sshTunnel, escapeshellcmd('df -P'));
        stream_set_blocking($streamedResponse, true);

        $response = stream_get_contents($streamedResponse);

        $response = explode("\n", $response);
        array_shift($response);
        array_pop($response);

        $diskUsageEntries = array_map(function ($line) {
            return array_values(array_filter(explode(" ", $line), function ($elm) {
                return mb_strlen($elm) > 0;
            }));
        }, $response);

        foreach ($diskUsageEntries as $index => &$diskUsageEntry) {
            if (!in_array($diskUsageEntry[5], self::PARTITIONS)) {
                unset($diskUsageEntries[$index]);
                continue;
            }

            $diskUsageEntry = [
                'partition'        => $diskUsageEntry[0],
                'totalSpace'       => $diskUsageEntry[1] * 1024,
                'usedSpace'        => $diskUsageEntry[2] * 1024,
                'freeSpace'        => $diskUsageEntry[3] * 1024,
                'usedSpacePercent' => $diskUsageEntry[4],
                'mountPoint'       => $diskUsageEntry[5]
            ];
        }

        return $diskUsageEntries;
    }

    /**
     * @return array
     */
    public function getMemoryUsage()
    {
        $sshTunnel = $this->rootAccess->getSshTunnel();

        $streamedResponse = ssh2_exec($sshTunnel, escapeshellcmd('cat /proc/meminfo'));
        stream_set_blocking($streamedResponse, true);

        $response = stream_get_contents($streamedResponse);
        $response = explode("\n", $response);

        $memoryUsageEntries = array_map(function ($line) {
            $explodedLine    = explode(":", $line);
            $explodedLine    = array_map('trim', $explodedLine);
            if (count($explodedLine) === 2) {
                $explodedLine[1] = explode(' ', $explodedLine[1])[0] * 1024;
            }

            return array_values(array_filter($explodedLine, function ($elm) {
                return mb_strlen($elm) > 0;
            }));
        }, $response);

        return [
            'used'  => $memoryUsageEntries[0][1] - $memoryUsageEntries[2][1],
            'free'  => $memoryUsageEntries[2][1],
            'total' => $memoryUsageEntries[0][1]
        ];
    }

    /**
     * @param bool $fast
     *
     * @return array
     */
    public function getCPUUsage($fast = false)
    {
        $sshTunnel = $this->rootAccess->getSshTunnel();

        $command = "cat <(mpstat -P ALL 1 1)";

        $streamedResponse = ssh2_exec($sshTunnel, $command);
        stream_set_blocking($streamedResponse, true);

        $data = explode("\n", stream_get_contents($streamedResponse));
        $data = array_map(function($datum) {
            $exploded = explode(" ", $datum);
            return array_values(array_filter($exploded, 'strlen'));
        }, $data);
        //-- Process header
        array_shift($data);
        //-- Blank line
        array_shift($data);
        //-- Table header
        array_shift($data);
        //-- Average ALL
        array_shift($data);

        $cores = [];
        foreach ($data as $core) {
            //-- We're done
            if (empty($core)) {
                break;
            }

            $cores[] = 100 - floatval(str_replace(',', '.', $core[11]));
        }

        return $cores;
    }
}
