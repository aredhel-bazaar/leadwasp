<?php

namespace App\Service;

use App\Entity\Media;
use Symfony\Component\Routing\RouterInterface;
use Twig_Environment;
use Twig_SimpleFilter;

class FsUtilityService extends \Twig_Extension
{
    const SUPPORTS_THUMBNAIL_MIME = [
        'image/',
        'video/',
    ];

    /**
     * @var SshAccessService
     */
    private $accessService;
    /**
     * @var RouterInterface
     */
    private $router;


    public function __construct(SshAccessService $accessService, RouterInterface $router)
    {

        $this->accessService = $accessService;
        $this->router        = $router;
    }


    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new Twig_SimpleFilter('absolute_path', [$this, 'getAbsolutePath']),
        ];
    }


    public function getFunctions()
    {
        return [
            new \Twig_Function('breadcrumb', [$this, 'generateBreadcrumb'], [
                'needs_environment' => true
            ]),
            new \Twig_Function('mediaTypeToFas', [$this, 'mediaTypeToFas']),
            new \Twig_Function('supportsThumbnail', [$this, 'supportsThumbnail']),
            new \Twig_Function('isFile', [$this, 'isFile'])
        ];
    }


    /**
     * @param string $path
     *
     * @return string
     */
    public function getAbsolutePath(string $path)
    {
        $path      = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $path);
        $parts     = array_filter(explode(DIRECTORY_SEPARATOR, $path), 'strlen');
        $absolutes = [];
        foreach ($parts as $part) {
            if ('.' == $part) continue;
            if ('..' == $part) {
                array_pop($absolutes);
            } else {
                $absolutes[] = $part;
            }
        }
        return implode(DIRECTORY_SEPARATOR, $absolutes);
    }


    /**
     * @param Twig_Environment $env
     * @param string           $path
     *
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws \Exception
     */
    public function generateBreadcrumb(Twig_Environment $env, string $path)
    {
        $path         = $this->getAbsolutePath($path);
        $explodedPath = explode(DIRECTORY_SEPARATOR, $path);
        if (empty($explodedPath) || empty($explodedPath[0])) {
            $explodedPath = [];
        }

        $pathList    = [];
        $currentPath = '';
        foreach ($explodedPath as $item) {
            $currentPath .= '/' . $item;

            $media = $this->accessService->getMedia($currentPath);

            if ($this->isFile($media->getMimeType())) {
                $href = $this->router->generate('display-media', ['path' => $currentPath]);
            } else {
                $href = $this->router->generate('homepage', ['path' => $currentPath]);
            }

            $pathList[] = [
                'href' => $href,
                'name' => $item
            ];
        }

        return $env->render('blocks/breadcrumb.html.twig', [
            'pathList' => $pathList
        ]);
    }


    /**
     * @param string $MediaType
     *
     * @return string
     */
    public function mediaTypeToFas(string $MediaType)
    {
        switch ($MediaType) {
            case Media::TYPE_DIRECTORY:
                return 'folder';

            case Media::TYPE_FILE:
                return 'file';

            case Media::TYPE_TEXT:
                return 'file-alt';

            case Media::TYPE_IMAGE:
                return 'file-image';

            case Media::TYPE_AUDIO:
                return 'file-audio';

            case Media::TYPE_VIDEO:
                return 'file-video';

            case Media::TYPE_PDF:
                return 'file-pdf';

            default:
                var_dump($MediaType);
        }
    }


    /**
     * @param string $mimeType
     *
     * @return bool
     */
    public function supportsThumbnail(string $mimeType)
    {
        foreach (self::SUPPORTS_THUMBNAIL_MIME as $item) {
            if (is_int(mb_strpos($mimeType, $item))) {
                return true;
            }
        }

        return false;
    }


    /**
     * @param string $mimeType
     *
     * @return bool
     */
    public function isFile(string $mimeType)
    {
        return ($mimeType !== 'inode/directory');
    }
}
