<?php

namespace App\Service;

class RsaService
{
    private $pubKeyFile;
    private $privateKeyFile;
    
    
    public function __construct(string $projetDir)
    {
        $this->pubKeyFile     = $projetDir.'/.ssh/public_key.pem';
        $this->privateKeyFile = $projetDir.'/.ssh/private_key.pem';
    }
    
    
    
    public function encrypt(string $str)
    {
        openssl_public_encrypt($str, $crypted, file_get_contents($this->pubKeyFile));
        
        return $crypted;
    }
    
    
    
    public function decrypt(string $str)
    {
        openssl_private_decrypt($str, $decrypted, file_get_contents($this->privateKeyFile));
    
        return $decrypted;
    }
}
