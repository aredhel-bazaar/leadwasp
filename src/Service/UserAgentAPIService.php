<?php

namespace App\Service;

use App\Entity\UserAgent;
use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\Cache\Adapter\SimpleCacheAdapter;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserAgentAPIService
{
    const CACHE_KEY_PREFIX = 'UserAgentAPIService.userAgent';
    const API_KEY          = 'ce04a970';
    
    /**
     * @var SimpleCacheAdapter
     */
    private $cacheAdapter;
    
    
    
    /**
     * UserAgentAPIService constructor.
     *
     * @param CacheItemPoolInterface $cacheAdapter
     */
    public function __construct(CacheItemPoolInterface $cacheAdapter)
    {
        $this->cacheAdapter = $cacheAdapter;
    }
    
    
    
    /**
     *
     * @param string $username
     *
     * @return mixed|\Symfony\Component\Cache\CacheItem
     */
    private function getCachedUserAgent(string $username)
    {
        return $this->cacheAdapter->getItem(self::CACHE_KEY_PREFIX.$username);
    }
    
    
    
    /**
     * @param string $username
     *
     * @return UserAgent
     * @throws \Exception
     */
    public function getUserAgent(string $username)
    {
        if (!$this->getCachedUserAgent($username)->isHit()) {
            $this->saveUserAgent($username);
        }
        
        return $this->getCachedUserAgent($username)->get();
    }



    /**
     * @param string $username
     *
     * @throws \Exception
     */
    public function saveUserAgent(string $username)
    {
//        $output = '{"data":{"ua_type":"Desktop","os_name":"Windows","os_version":"10","browser_name":"Firefox","browser_version":"58.0","engine_name":"Gecko","engine_version":"20100101"}}';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://useragentapi.com/api/v4/json/'.self::API_KEY.'/'.curl_escape($ch, $_SERVER['HTTP_USER_AGENT']));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $output = curl_exec($ch);


        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);
        curl_close($ch);

        $decodedOutput = json_decode($output, true);

        if (empty($decodedOutput) || $curl_errno != 0) {
            throw new \Exception("Unable to contact the API. ".$curl_error);
        }

        $cachedUserAgent = $this->getCachedUserAgent($username);
        $cachedUserAgent->set(new UserAgent($decodedOutput['data']));
        $this->cacheAdapter->save($cachedUserAgent);
    }
}
