<?php

namespace App\Service;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class TwigHelper extends AbstractExtension
{
    public function getFilters()
    {
        return array(
            new TwigFilter('sha1', 'sha1'),
        );
    }
}