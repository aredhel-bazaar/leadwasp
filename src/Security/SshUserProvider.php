<?php

namespace App\Security;

use App\Entity\User;
use App\Service\RsaService;
use App\Service\UserAgentAPIService;
use Exception;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class SshUserProvider implements UserProviderInterface
{
    const PREREQUISITES = [
        'ssh2_connect',
        'ssh2_auth_password',
    ];
    /**
     * @var string
     */
    private $operatingSystem;
    /**
     * @var RsaService
     */
    private $rsaService;
    /**
     * @var UserAgentAPIService
     */
    private $userAgentAPIService;
    /**
     * @var SshRootAccess
     */
    private $rootAccess;
    
    /**
     * AuthenticationService constructor.
     *
     * @param SshRootAccess       $rootAccess
     * @param RsaService          $rsaService
     * @param UserAgentAPIService $userAgentAPIService
     *
     * @throws Exception
     */
    public function __construct(
        SshRootAccess $rootAccess,
        RsaService $rsaService,
        UserAgentAPIService $userAgentAPIService
    ) {
        // Checking prerequisites
        foreach (self::PREREQUISITES as $prerequisite) {
            if (!function_exists($prerequisite)) {
                throw new Exception("Missing function $prerequisite");
            }
        }
        
        $this->operatingSystem     = php_uname('s');
        $this->rsaService          = $rsaService;
        $this->userAgentAPIService = $userAgentAPIService;
        $this->rootAccess          = $rootAccess;
    }
    
    /**
     * Authenticate the user against the system user manager
     *
     * @param string $user
     * @param string $password
     *
     * @return bool
     * @throws Exception
     */
    public function authenticate(string $user, string $password)
    {
        switch ($this->operatingSystem) {
            case 'Linux':
                $authenticationSuccess = $this->linuxAuthentication($user, $password);
                break;
            
            default:
                throw new Exception("This operating system is not supported by the authentication manager");
        }
        
        return $authenticationSuccess;
    }
    
    /**
     * @param string $user
     * @param string $password
     *
     * @return bool
     */
    private function linuxAuthentication(string $user, string $password)
    {
        $sshTunnel         = ssh2_connect('localhost', 22);
        $userAuthenticated = ssh2_auth_password($sshTunnel, $user, $password);
        
        unset($sshTunnel);
        
        return $userAuthenticated;
    }
    
    /**
     * Loads the user for the given username.
     *
     * This method must throw UsernameNotFoundException if the user is not
     * found.
     *
     * @param string $username The username
     *
     * @return UserInterface
     *
     * @throws UsernameNotFoundException if the user is not found
     */
    public function loadUserByUsername($username)
    {
        $sshTunnel = $this->rootAccess->getSshTunnel();
        $query     = @ssh2_exec($sshTunnel, "id $username");
        
        if ($query === false) {
            throw new UsernameNotFoundException();
        }
        
        $stream = ssh2_fetch_stream($query, SSH2_STREAM_STDIO);
        stream_set_blocking($stream, true);
        
        $result = stream_get_contents($stream);
        
        if (empty($result)) {
            throw new UsernameNotFoundException();
        }
        
        $user = new User();
        $user->setUsername($username);
        
        unset($sshTunnel, $query, $stream, $result);
        
        return $user;
    }
    
    /**
     * Refreshes the user.
     *
     * It is up to the implementation to decide if the user data should be
     * totally reloaded (e.g. from the database), or if the UserInterface
     * object can just be merged into some internal array of users / identity
     * map.
     *
     * @param UserInterface $user
     *
     * @return UserInterface
     *
     * @throws Exception
     */
    public function refreshUser(UserInterface $user)
    {
        $user->setUserAgent($this->userAgentAPIService->getUserAgent($user->getUsername()));
        
        return $user;
    }
    
    /**
     * Whether this provider supports the given user class.
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return $class instanceof User;
    }
}
