<?php

namespace App\Security;

class SshRootAccess
{
    /**
     * @var string
     */
    private $ssh_admin_username;
    /**
     * @var string
     */
    private $ssh_admin_password;
    private $sshTunnel;
    
    public function __construct(string $ssh_admin_username, string $ssh_admin_password)
    {
        $this->ssh_admin_username = $ssh_admin_username;
        $this->ssh_admin_password = $ssh_admin_password;
    }
    
    /**
     * @return resource
     */
    public function getSshTunnel()
    {
        if (is_null($this->sshTunnel)) {
            $this->sshTunnel = ssh2_connect('localhost', 22);
    
            @ssh2_auth_password($this->sshTunnel, $this->ssh_admin_username, $this->ssh_admin_password);
        }
        
        return $this->sshTunnel;
    }
}
