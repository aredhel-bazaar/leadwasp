<?php

namespace App\Security;

use App\Entity\UserAgent;
use App\Entity\User;
use App\Form\LoginForm;
use App\Service\RsaService;
use App\Service\UserAgentAPIService;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

/**
 * Class Guard Authenticator. A chaque appel d'une page, si on détecte une tentative de connexion, on vérifie que ses
 * credentials sont bons
 *
 * Class LoginFormAuthenticator
 * @package AppBundle\Service
 */
class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var FlashBagInterface
     */
    private $flashBag;
    /**
     * @var SshUserProvider
     */
    private $authentication;
    
    const LOGIN_ROUTE_NAME      = 'security_login';
    const CUSTOM_LOGIN_ERROR    = 999;
    /**
     * @var FormInterface
     */
    private $form;
    /**
     * @var RsaService
     */
    private $rsaService;
    /**
     * @var UserAgentAPIService
     */
    private $userAgentAPIService;


    /**
     * LoginFormAuthenticator constructor.
     *
     * @param FormFactoryInterface         $formFactory
     * @param RouterInterface              $router
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param FlashBagInterface            $flashBag
     * @param SshUserProvider              $authentication
     * @param RsaService                   $rsaService
     * @param UserAgentAPIService          $userAgentAPIService
     */
    public function __construct(FormFactoryInterface $formFactory, RouterInterface $router, UserPasswordEncoderInterface $passwordEncoder, FlashBagInterface $flashBag, SshUserProvider $authentication, RsaService $rsaService, UserAgentAPIService $userAgentAPIService)
    {
        $this->formFactory         = $formFactory;
        $this->router              = $router;
        $this->passwordEncoder     = $passwordEncoder;
        $this->flashBag            = $flashBag;
        $this->authentication      = $authentication;
        $this->rsaService          = $rsaService;
        $this->userAgentAPIService = $userAgentAPIService;
    }
    
    
    
    /**
     * @param Request $request
     *
     * @return mixed|null
     */
    public function getCredentials(Request $request)
    {
        /** @var User $user */
        $user = $this->form->getData();
        
        $credentials['username']       = $user->getUsername();
        $credentials['password']       = $user->getPlainPassword();

        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['username']
        );

        return $credentials;
    }
    
    
    
    /**
     * @param mixed                 $credentials
     * @param UserProviderInterface $userProvider
     *
     * @return UserInterface
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        return $userProvider->loadUserByUsername($credentials['username']);
    }
    
    
    
    /**
     * @param mixed         $credentials
     * @param UserInterface $user
     *
     * @return bool
     * @throws \Exception
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        $isAuthenticated = $this->authentication->authenticate($credentials['username'], $credentials['password']);

        if ($isAuthenticated) {
            $user->setPassword($this->rsaService->encrypt($credentials['password']));
        }

        return $isAuthenticated;
    }

    /**
     * Retourne l'url de connexion
     */
    protected function getLoginUrl()
    {
        return $this->router->generate(self::LOGIN_ROUTE_NAME);
    }

    /**
     * Cette méthode est appelée lorsque le process de connexion a réussi. Défini l'action à suivre lorsque
     * l'utilisateur s'est connecté
     *
     * @param Request        $request
     * @param TokenInterface $token
     * @param string         $providerKey
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // Récupération de certaines informations, comme le navigateur
        try {
            $this->userAgentAPIService->saveUserAgent($token->getUsername());
        } catch (\Exception $e) {}
        //

        $redirect = $request->headers->get('referer', $this->router->generate('homepage'));
        if ($this->router->match($redirect) === $this->router->generate('security_login')) {
            $redirect = $this->router->generate('homepage');
        }

        return new RedirectResponse($redirect);
    }

    /**
     * En cas d'erreur, on ajoute l'exception dans le FlashBag et on renvoie l'utilisateur sur la page de connexion
     *
     * @param Request                 $request
     * @param AuthenticationException $authenticationException
     *
     * @return RedirectResponse
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $authenticationException)
    {
        if( $authenticationException->getCode() == self::CUSTOM_LOGIN_ERROR ) {
            $this->flashBag->add('error', $authenticationException->getMessage());
        } else {
            if ($request->getSession() instanceof SessionInterface) {
                $request->getSession()->set(Security::AUTHENTICATION_ERROR, $authenticationException);
            }
        }

        return new RedirectResponse($this->getLoginUrl());
    }
    
    
    
    /**
     * Does the authenticator support the given Request?
     *
     * If this returns false, the authenticator will be skipped.
     *
     * @param Request $request
     *
     * @return bool
     */
    public function supports(Request $request)
    {
        if (!$request->get('_route') === self::LOGIN_ROUTE_NAME && $request->isMethod('POST')) {
            return false;
        }
    
        $this->form = $this->formFactory->create(LoginForm::class);
        $this->form->handleRequest($request);
    
        if (!$this->form->isSubmitted() || !$this->form->isValid()) {
            return false;
        }
        
        return true;
    }
}
