<?php

namespace App\Entity;

class UserAgent
{
    /**
     * @var string
     */
    private $userAgentType;
    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $version;
    
    const MIME_TYPE_AUDIO_MP3    = 'audio/mp3';
    const MIME_TYPE_AUDIO_AAC    = 'audio/aac';
    const MIME_TYPE_AUDIO_FLAC   = 'audio/flac';
    const MIME_TYPE_AUDIO_VORBIS = 'audio/vorbis';
    
    const MIME_TYPES = [
        self::MIME_TYPE_AUDIO_MP3,
        self::MIME_TYPE_AUDIO_AAC,
        self::MIME_TYPE_AUDIO_FLAC,
        self::MIME_TYPE_AUDIO_VORBIS,
    ];
    
    
    
    /**
     * UserAgent constructor.
     *
     * @param array $data
     */
    public function __construct(?array $data)
    {
        $this->userAgentType = $data['ua_type'] ?? $data['userAgentType'] ?? null;
        $this->name          = $data['browser_name'] ?? $data['name'] ?? null;
        $this->version       = $data['browser_version'] ?? $data['version'] ?? 0;
    }

    /**
     * @return string
     */
    public function getUserAgentType(): string
    {
        return $this->userAgentType;
    }

    /**
     * @param string $userAgentType
     *
     * @return UserAgent
     */
    public function setUserAgentType(?string $userAgentType): UserAgent
    {
        $this->userAgentType = $userAgentType;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return UserAgent
     */
    public function setName(?string $name): UserAgent
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     *
     * @return UserAgent
     */
    public function setVersion(int $version): UserAgent
    {
        $this->version = $version;

        return $this;
    }
    
    
    
    /**
     * @param string $format
     *
     * @return integer
     */
    public function supportsMimeType(string $format)
    {
        switch ($format) {
            case 'audio/mpeg':
            case 'audio/mp3':
                return (int) (
                    $this->userAgentType == 'Desktop' && $this->name == 'Firefox' && $this->version >= 22 ||
                    $this->userAgentType == 'Desktop' && $this->name == 'Chrome' && $this->version >= 4 ||
                    $this->userAgentType == 'Desktop' && $this->name == 'IE' && $this->version >= 9 ||
                    $this->userAgentType == 'Desktop' && $this->name == 'Edge'
                );
                break;
                
            case 'audio/vorbis':
            case 'audio/ogg':
                return (int) (
                    $this->userAgentType == 'Desktop' && $this->name == 'Firefox' && $this->version >= 3.5 ||
                    $this->userAgentType == 'Desktop' && $this->name == 'Chrome' && $this->version >= 4
                );
                break;
                
            case 'audio/aac':
                return (
                    $this->userAgentType == 'Desktop' && $this->name == 'Chrome' && $this->version >= 12 ||
                    $this->userAgentType == 'Desktop' && $this->name == 'IE' && $this->version >= 9 ||
                    $this->userAgentType == 'Desktop' && $this->name == 'Edge'
                );
                break;
                
            case 'audio/flac':
            case 'audio/x-flac':
                return (int) (
                    $this->userAgentType == 'Desktop' && $this->name == 'Firefox' && $this->version >= 51 ||
                    $this->userAgentType == 'Desktop' && $this->name == 'Chrome' && $this->version >= 56 ||
                    $this->userAgentType == 'Desktop' && $this->name == 'Edge' && $this->version >= 16
                );
                break;

            case 'video/x-flv':
                return 2;
                break;

            case 'video/mp4':
            case 'video/mpeg':
                return (int) (
                    $this->userAgentType == 'Desktop' && $this->name == 'Firefox' && $this->version >= 35 ||
                    $this->userAgentType == 'Desktop' && $this->name == 'Chrome' && $this->version >= 4 ||
                    $this->userAgentType == 'Desktop' && $this->name == 'IE' && $this->version >= 9 ||
                    $this->userAgentType == 'Desktop' && $this->name == 'Edge'
                );
                break;

            case 'video/x-msvideo':
            case 'video/x-ms-asf':
                return 0;
                break;
        }
        
        return 0;
    }
    
    
    
    /**
     * @return array
     */
    public function getSupportedMimeTypes()
    {
        $supportedMimeTypes = [];
        foreach (self::MIME_TYPES as $mimeType) {
            if ($this->supportsMimeType($mimeType)) {
                $supportedMimeTypes[] = $mimeType;
            }
        }
        
        return $supportedMimeTypes;
    }
}
