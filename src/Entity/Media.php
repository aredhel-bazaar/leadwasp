<?php

namespace App\Entity;

class Media
{
    private $title;
    private $path;
    private $filename;
    private $mimeType;
    private $codec;
    private $width;
    private $height;
    private $filesize;
    private $encoding;
    private $duration;
    private $thumbnail;
    private $modificationDate;
    /**
     * @var boolean
     */
    private $supportLevel;
    /**
     * @var string
     */
    private $extension;
    /**
     * @var array
     */
    private $meta = [];


    const TYPE_ARCHIVE   = 'archive';
    const TYPE_AUDIO     = 'audio';
    const TYPE_DIRECTORY = 'directory';
    const TYPE_FILE      = 'file';
    const TYPE_IMAGE     = 'image';
    const TYPE_PDF       = 'pdf';
    const TYPE_TEXT      = 'text';
    const TYPE_VIDEO     = 'video';


    /**
     * Media constructor.
     *
     * @param array|null $data
     *
     * @throws \Exception
     */
    public function __construct(array $data = [])
    {
        foreach ($data as $prop => $datum) {
            if (!property_exists(self::class, $prop)) {
                $this->meta[$prop] = $datum;
            } else {
                $this->$prop = $datum;
            }
        }
    }


    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }


    /**
     * @param mixed $title
     *
     * @return Media
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }


    /**
     * @param mixed $path
     *
     * @return Media
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }


    /**
     * @param mixed $filename
     *
     * @return Media
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }


    /**
     * @param mixed $mimeType
     *
     * @return Media
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getCodec()
    {
        return $this->codec;
    }


    /**
     * @param mixed $codec
     *
     * @return Media
     */
    public function setCodec($codec)
    {
        $this->codec = $codec;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }


    /**
     * @param mixed $width
     *
     * @return Media
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }


    /**
     * @param mixed $height
     *
     * @return Media
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getFilesize()
    {
        return $this->filesize;
    }


    /**
     * @param mixed $filesize
     *
     * @return Media
     */
    public function setFilesize($filesize)
    {
        $this->filesize = $filesize;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getEncoding()
    {
        return $this->encoding;
    }


    /**
     * @param mixed $encoding
     *
     * @return Media
     */
    public function setEncoding($encoding)
    {
        $this->encoding = $encoding;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }


    /**
     * @param mixed $duration
     *
     * @return Media
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }


    /**
     * @param mixed $thumbnail
     *
     * @return Media
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }


    public function setSupportLevel($supportLevel)
    {
        $this->supportLevel = $supportLevel;
    }


    public function isSupportLevel()
    {
        return $this->supportLevel;
    }

    /**
     * @return mixed
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * @param mixed $modificationDate
     *
     * @return Media
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }


    public function getExtension()
    {
        if (empty($this->extension)) {
            $this->extension = mb_strtolower(pathinfo($this->path, PATHINFO_EXTENSION));
        }

        return $this->extension;
    }

    public function getMeta()
    {
        return $this->meta;
    }


    public function getType()
    {
        $typeInfo = explode('/', $this->getMimeType());
        $fileExt  = $this->getExtension();

        switch ($typeInfo[0]) {
            case 'inode':
                return self::TYPE_DIRECTORY;

            case 'text':
                return self::TYPE_TEXT;

            case 'image':
                return self::TYPE_IMAGE;

            case 'audio':
                return self::TYPE_AUDIO;

            case 'video':
            case 'x-ms-asf':
                if ($fileExt === 'wma') {
                    return self::TYPE_AUDIO;
                } else {
                    return self::TYPE_VIDEO;
                }
                break;

            case 'application':
                if ($typeInfo[1] === 'x-rar' || $typeInfo[1] === 'zip') {
                    return self::TYPE_ARCHIVE;
                }

                if ($typeInfo[1] === 'pdf') {
                    return self::TYPE_PDF;
                }

                if ($typeInfo[1] === 'x-bittorrent') {
                    return self::TYPE_TEXT;
                }

                if ($typeInfo[1] === 'octet-stream' && $fileExt === 'mp3') {
                    return self::TYPE_AUDIO;
                }

            default:
                return self::TYPE_FILE;
        }
    }


    /**
     * @return boolean
     */
    public function isImage()
    {
        return preg_match('#^image\/#i', $this->getMimeType()) > 0;
    }


    /**
     * @return boolean
     */
    public function isVideo()
    {
        return preg_match('#^video\/#i', $this->getMimeType()) > 0;
    }


    /**
     * @return boolean
     */
    public function isStreamable()
    {
        return in_array($this->getType(), [self::TYPE_AUDIO, self::TYPE_VIDEO]);
    }


    /**
     * @return boolean
     */
    public function hasThumbnailSupport()
    {
        return in_array($this->getType(), [self::TYPE_VIDEO, self::TYPE_IMAGE, self::TYPE_AUDIO, self::TYPE_DIRECTORY]);
    }


    /**
     * @return boolean
     */
    public function isDirectory()
    {
        return $this->getType() === self::TYPE_DIRECTORY;
    }


    /**
     * @return boolean
     */
    public function getName()
    {
        return $this->getTitle() . ($this->isDirectory() ? '' : '.' . $this->getExtension());
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function isType($type)
    {
        switch ($type) {
            case self::TYPE_VIDEO:
                return $this->isVideo();

            case self::TYPE_IMAGE:
                return $this->isImage();

            default:
                return $this->getType() === $type;
        }
    }
}
