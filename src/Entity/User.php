<?php

namespace App\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $password;
    /**
     * @var string
     */
    private $plainPassword;
    /**
     * @var UserAgent
     */
    private $userAgent;
    
    
    
    /**
     * @return array
     */
    public function getRoles()
    {
        return [
            'ROLE_USER'
        ];
    }
    
    
    
    /**
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }
    
    
    
    /**
     * @param null|string $password
     *
     * @return self
     */
    public function setPassword(?string $password)
    {
        $this->password = $password;
        
        return $this;
    }
    
    
    
    /**
     * @return string|null The salt
     */
    public function getSalt()
    {
        return;
    }
    
    
    
    /**
     * @return string The username
     */
    public function getUsername()
    {
        return $this->username;
    }
    
    
    
    /**
     * @param null|string $username
     *
     * @return self
     */
    public function setUsername(?string $username)
    {
        $this->username = $username;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }
    
    
    
    /**
     * @param null|string $password
     *
     * @return $this
     */
    public function setPlainPassword(?string $password)
    {
        $this->plainPassword = $password;
        
        return $this;
    }
    
    
    
    public function eraseCredentials() {
        $this->plainPassword = null;
    }
    
    
    
    /**
     * @return UserAgent|null
     */
    public function getUserAgent(): ?UserAgent
    {
        return $this->userAgent;
    }
    
    
    
    /**
     * @param UserAgent|null $userAgent
     *
     * @return User
     */
    public function setUserAgent(?UserAgent $userAgent): User
    {
        $this->userAgent = $userAgent;
        
        return $this;
    }
}
