<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class LoginForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label'         => 'user.login.label',
                'attr'          => array('placeholder' => 'user.login.label'),
                'required'      => true,
                'constraints'   => [
                    new NotBlank()
                ]
            ])
            ->add('plainPassword', PasswordType::class, [
                'label'         => 'user.password.label',
                'attr'          => array('placeholder' => 'user.password.label'),
                'required'      => true,
                'constraints'   => [
                    new NotBlank()
                ]
            ])
        ;
    }
    
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => User::class
        ));
    }
}
