<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;

class ExceptionListener
{
    /**
     * @var RouterInterface
     */
    private $router;
    
    
    
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }
    
    
    
    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if ($event->getException() instanceof NotFoundHttpException) {
            $data = [];
            
            if (!empty($event->getRequest()->getPathInfo())) {
                $data['path'] = $event->getRequest()->getPathInfo();
            }
            
            $event->setResponse(new RedirectResponse(
                $this->router->generate('homepage', $data)
            ));
        }
    }
}
