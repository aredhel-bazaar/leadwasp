<?php

namespace App\Test\Entity;

use App\Entity\UserAgent;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class UserAgentTest extends TestCase
{
    /**
     * @test
     */
    public function UserAgentBrowserIsFirefox()
    {
        $userAgent = new UserAgent(['name' => 'Firefox']);

        $this->assertSame('Firefox', $userAgent->getName());
    }



    /**
     * @test
     */
    public function UserAgentBrowserVersionIsGreaterThan28()
    {
        $userAgent = new UserAgent(['version' => '31.0']);

        $this->assertGreaterThan(28, $userAgent->getVersion());
    }



    /**
     * @test
     */
    public function UserAgentIsBrowser()
    {
        $userAgent = new UserAgent(['userAgentType' => 'Desktop']);

        $this->assertSame('Desktop', $userAgent->getUserAgentType());
    }



    /**
     * @test
     */
    public function InternetExplorer10DesktopSupportsAudioMpeg()
    {
        $userAgent = new UserAgent([
            'userAgentType' => 'Desktop',
            'version'       => '10',
            'name'          => 'IE',
        ]);

        $this->assertSame(1, $userAgent->supportsMimeType('audio/mpeg'));
    }
}