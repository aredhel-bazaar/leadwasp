<?php

namespace App\Test\Entity;

use App\Entity\Media;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class MediaTest extends TestCase
{

    public function mimeTypeProvider()
    {
        return [
            ['/path/to/audio.mp3',      'audio/mpeg',           Media::TYPE_AUDIO],
            ['/path/to/video.mp4',      'video/mpeg',           Media::TYPE_VIDEO],
            ['/path/to/archive.rar',    'application/x-rar',    Media::TYPE_ARCHIVE],
            ['/path/to/facture.pdf',    'application/pdf',      Media::TYPE_PDF],
            ['/path/to/audio.wma',      'video/x-ms-asf',       Media::TYPE_AUDIO],
        ];
    }


    /**
     * @test
     * @dataProvider mimeTypeProvider
     *
     * @param string $path
     * @param string $mimeType
     * @param string $assertType
     */
    public function mediaTypeIsCorrect(string $path, string $mimeType, string $assertType)
    {
        $media = new Media([
            'path'     => $path,
            'mimeType' => $mimeType
        ]);
        
        $type = $media->getType();
        
        $this->assertEquals($assertType, $type);
    }
}
